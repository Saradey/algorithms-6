package com.company;

public class Main {


    public static void main(String[] args) {

        //Task 1
        System.out.println("");

        HashFunctionTask1 hashFunctionTask1 = new HashFunctionTask1();

        hashFunctionTask1.setAge(24);
        hashFunctionTask1.setName("Test test");
        hashFunctionTask1.setPosition("Employee");
        hashFunctionTask1.setSalary(5.5);


        HashFunctionTask1 hashFunctionTask2 = new HashFunctionTask1();

        hashFunctionTask2.setAge(23);
        hashFunctionTask2.setName("TEST2 TEST2");
        hashFunctionTask2.setPosition("Employee2");
        hashFunctionTask2.setSalary(123);

        System.out.println("Test test Hash code:" + hashFunctionTask1.hashCode());
        System.out.println("TEST2 TEST2 Hash code:" + hashFunctionTask2.hashCode());
        //end Task 1


        //Task 2

        System.out.println("\nTask 2");

        Tree<Integer> tree = new Tree<>();

        tree.insertValue(10);

        tree.insertValue(11);

        tree.insertValue(10);

        tree.insertValue(12);

        tree.insertValue(8);

        tree.insertValue(9);

        tree.insertValue(7);

        tree.print();

        System.out.println("Search:" + tree.search(11));


        //end Task 2


    }


    static class Tree<T> {

        Node<T> root = null;


        public void insertValue(T value) {
            Node node = new Node();
            node.value = value;


            if (root == null) {
                root = node;
            } else {
                Node current = root;
                while (true) {
                    //проверка на левый
                    if (current.value.hashCode() > value.hashCode()) {
                        if (current.leftValue != null) {
                            current = current.leftValue;
                        } else {
                            current.leftValue = new Node();
                            current.leftValue.value = value;
                            return;
                        }
                    } else {
                        //проверка на правый
                        if (current.rightValue != null) {
                            current = current.rightValue;
                        } else {
                            current.rightValue = new Node();
                            current.rightValue.value = value;
                            return;
                        }
                    }

                }

            }

        }


        public void print() {
            print(root);
        }


        private void print(Node node) {
            node.print();

            if (node.leftValue != null) {
                print(node.leftValue);
            }

            if (node.rightValue != null) {
                print(node.rightValue);
            }
        }


        public T search(T value) {

            if (root.value.hashCode() == value.hashCode())
                return (T) root.value;


            Node current = root;
            T result = value;

            search(current, result);

            /*
            while (true) {
                if (current.value.hashCode() > value.hashCode()) {
                    if (current.leftValue != null) {

                        if(current.leftValue.value == value){
                            result = (T)current.leftValue.value;
                            break;
                        }

                    }
                } else {

                    if (current.rightValue != null) {
                        if(current.rightValue.value == value){
                            result = (T)current.rightValue.value;
                            break;
                        }
                    }

                }
            }
            */

            return result;
        }



        private void search(Node node, T value) {


            if (node.leftValue != null) {
                if(node.value.hashCode() == value.hashCode()){
                    value = (T)node.value;
                    return;
                }

                search(node.leftValue, value);
            }


            if (node.rightValue != null) {
                if(node.value.hashCode() == value.hashCode()){
                    value = (T)node.value;
                    return;
                }

                search(node.rightValue, value);
            }
        }

    }


    static class Node<T> {
        T value = null;

        private Node leftValue;
        private Node rightValue;


        public void print() {

            String teamp = "";

//            if(leftValue != null)
//                teamp += " leftValue";
//            else teamp += " leftValue null";
//
//            if(rightValue != null)
//                teamp += " rightValue";
//            else teamp += " rightValue null";
//
            System.out.println(value.toString() + teamp);
        }
    }


    static class HashFunctionTask1 {

        private String name;
        private int age;
        private double salary;
        private String position;


        @Override
        public int hashCode() {
            int result = 0;

            final int TEAMP = 32;
            result = age + result * TEAMP;
            result = (int) salary + result * TEAMP;

            for (Character it : name.toCharArray()) {
                result = result + it * TEAMP;
            }

            for (Character it : position.toCharArray()) {
                result = result + it * TEAMP;
            }

            return result;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public double getSalary() {
            return salary;
        }

        public void setSalary(double salary) {
            this.salary = salary;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }
    }


}
